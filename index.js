const express = require('express');
const app = express();
const http = require('http');
var cors = require('cors');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, {
    cors: {
        origin: "*",
        credentials: true
    }
});
const PORT = process.env.PORT || 3000;
app.use(cors())
app.get('/', (req, res) => {
    res.send('ok');
});

io.on('connection', (socket) => {
    socket.on('poll', (msg) => {
        io.emit('poll', msg);
    });
});


server.listen(PORT, () => {
    console.log('listening on *:3000');
});